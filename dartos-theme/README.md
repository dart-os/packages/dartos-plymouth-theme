##<font color="#55aa00"><b>dartos-plymouth-theme</b></font>

* __`Version:`__ 0.1~alpha-1
* __`Date:`__ 27.03.2023
* __`License:`__  [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt)
* __`Copyright:`__ Copyright (C) 2023 Arijit Bhowmick <cybersec.arijitbhowmick@gmail.com>
* __`Author:`__ [Arijit Bhowmick](https://sys41x4.github.io/)

---------------------------------

## Package:
dartos-plymouth-theme.deb


---------------------------------
### Changelog:
>
___`v0.1~alpha (26.03.2023)`___
>
* `First alpha release`

>
___`v0.1~alpha-1 (27.03.2023)`___
>
* `First alpha revisioned release`









